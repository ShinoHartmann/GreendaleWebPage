/**
* Sort products by filter
*/

function filterProduct(showClass, hideClass) {
	$(showClass).removeClass('hide');
	$(hideClass).addClass('hide');
}

$('#book').click(function() {
	filterProduct('.book', '.cloth');
	$('#book').addClass('active');
	$('#cloth').removeClass('active');
});

$('#cloth').click(function() {
	filterProduct('.cloth', '.book');
	$('#cloth').addClass('active');
	$('#book').removeClass('active');
});

$('#all').click(function() {
	$('.book').removeClass('hide');
	$('.cloth').removeClass('hide');
	$('#cloth').removeClass('active');
	$('#book').removeClass('active');
});

/**
* Paypal open mini screent for payment
*/
for(var i = 1; i <= 9; i++) {
	new PAYPAL.apps.DGFlowMini({trigger: 'submitBtn' + i});
}